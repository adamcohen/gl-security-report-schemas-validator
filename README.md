# gl-security-report-schemas-validator

## Purpose

Answers the question: does my report validate against the [GitLab Security Report Schemas](https://gitlab.com/gitlab-org/security-products/security-report-schemas/)?

## Usage

```shell
docker run -it --rm -v "$PWD:/ci-project-dir" -w /ci-project-dir -e SECURE_LOG_LEVEL=debug \
  registry.gitlab.com/adamcohen/gl-security-report-schemas-validator/main -f <path-of-report-to-validate>
```

Example:

```shell
wget https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/raw/v4.3.0/qa/expect/js-npm/default/gl-dependency-scanning-report.json

docker run --platform linux/amd64 -it --rm -v "$PWD:/ci-project-dir" -w /ci-project-dir -e SECURE_LOG_LEVEL=debug \
  registry.gitlab.com/adamcohen/gl-security-report-schemas-validator/main -f gl-dependency-scanning-report.json -o all -s 14.0.0

2023-08-16 18:56:23.111627 I Main -- Validating against schema version in report: '14.0.4'
2023-08-16 18:56:23.114788 I Main -- Validating file 'gl-dependency-scanning-report.json' against schema version '14.0.4' with schema file '/usr/share/security-report-schemas/v14.0.4/dependency-scanning-report-format.json'
2023-08-16 18:56:23.114953 D Main -- Command: jsonschema --instance "gl-dependency-scanning-report.json" "/usr/share/security-report-schemas/v14.0.4/dependency-scanning-report-format.json" 2>&1
2023-08-16 18:56:23.733742 I Main -- File 'gl-dependency-scanning-report.json' validates successfully against schema '14.0.4'
```
