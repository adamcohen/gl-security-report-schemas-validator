FROM debian:buster-slim AS schemas

RUN apt-get update && apt-get install -y git curl
WORKDIR /build

RUN /bin/bash -c "$(curl -fsSL https://gitlab.com/gitlab-org/security-products/analyzers/integration-test/-/raw/ac9c849/scripts/export_security-report-schemas_dist.sh)"

FROM alpine:3.18.3

COPY --from=schemas /build/security-report-schemas /usr/share/security-report-schemas

RUN apk update && apk add jq py3-jsonschema ruby

RUN gem install bundler

RUN mkdir /tmp/app

WORKDIR /tmp/app

COPY main.rb Gemfile /tmp/app/

RUN bundle

ENTRYPOINT ["/tmp/app/main.rb"]
