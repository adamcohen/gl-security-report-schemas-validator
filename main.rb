#!/usr/bin/env ruby

require 'optparse'
require 'hashie'
require 'semantic_logger'
require 'json'

SemanticLogger.default_level = ENV.fetch('SECURE_LOG_LEVEL', :info)

class NoPidFormatter < SemanticLogger::Formatters::Color
  # Leave out the pid and thread_name
  def pid; end
  def thread_name; end

  # only log the process info if we encounter an error
  def process_info
    super if log.level == :error
  end
end

SemanticLogger.add_appender(io: $stdout, formatter: NoPidFormatter.new)

class Main
  include SemanticLogger::Loggable

  FILE_NAME_TO_SCHEMA_NAME = {
    'gl-dependency-scanning-report.json' => 'dependency-scanning-report-format.json',
    'gl-sast-report.json' => 'sast-report-format.json',
    'gl-cluster-image-scanning-report.json' => 'cluster-image-scanning-report-format.json',
    'gl-secret-detection-report.json' => 'secret-detection-report-format.json',
    'gl-dast-report.json' => 'dast-report-format.json',
    'gl-coverage-fuzzing.json' => 'coverage-fuzzing-report-format.json',
    'gl-container-scanning-report.json' => 'container-scanning-report-format.json'
  }.freeze

  def initialize(file:, schema_versions:, output_format:)
    @file = file
    @schema_versions = schema_versions
    @output_format = output_format

    unless file
      logger.error("File name is required")
      exit(1)
    end

    unless File.exist?(file)
      logger.error("File '#{file}' not found")
      exit(1)
    end

    @schema_name = FILE_NAME_TO_SCHEMA_NAME[file]

    unless schema_name
      logger.error("Schema for '#{file}' not found")
      exit(1)
    end

    parsed_report = JSON.parse(File.read(file))
    @report_schema_version = parsed_report['version']
  end

  attr_reader :file, :schema_versions, :schema_name, :report_schema_version, :output_format

  def validate
    unless schema_versions
      logger.info("Validating against schema version in report: '#{report_schema_version}'")
      @schema_versions = [report_schema_version]
    end

    if schema_versions == ['all']
      @schema_versions = Dir['/usr/share/security-report-schemas/*'].map do |dir|
        File.basename(dir).delete_prefix('v')
      end

      schema_versions.sort_by! { |v| Gem::Version.new(v) }

      logger.info("Validating against schema versions: #{schema_versions.join(', ')}")
    end

    schema_versions.each do |schema_version|
      schema_path = "/usr/share/security-report-schemas/v#{schema_version}/#{schema_name}"
      unless File.exist?(schema_path)
        logger.error("Schema '#{schema_path}' not found, skipping")
        next
      end

      cmd = %(jsonschema --instance "#{file}" "#{schema_path}" 2>&1)

      logger.info("Validating file '#{file}' against schema version '#{schema_version}' with schema file '#{schema_path}'")
      logger.debug("Command: #{cmd}")
      validation_response = `#{cmd}`

      if $?.exitstatus != 0
        logger.info("File '#{file}' does not validate against schema '#{schema_version}'")
        logger.error(validation_response) if output_format == 'all'
      else
        logger.info("File '#{file}' validates successfully against schema '#{schema_version}'")
      end
    end
  end

  def self.parse(options)
    args = Hashie::Mash.new(schema_versions: nil, file: nil, output_format: 'summary')

    opt_parser = OptionParser.new do |opts|
      opts.banner = 'Usage: main.rb [options]'

      opts.on(
        '-fFILE', '--file=FILE', "File to validate"
      ) do |file|
        args.file = file
      end

      opts.on(
        '-oOUTPUT', '--output-format=OUTPUT', "Output format. One of 'all' or 'summary'. Default: 'summary'"
      ) do |output_format|
        args.output_format = output_format
      end

      opts.on(
        '-s x,y,z', '--schema-versions', <<~HEREDOC,
          Comma separated list of schema versions to validate against. Example: 14.0.4,15.0.4. Can also pass 'all' to validate against all schemas.
                                               (default: validates against schema version listed in report)
        HEREDOC
        Array
      ) do |schema_versions|
        args.schema_versions = schema_versions
      end

      opts.on('-h', '--help', 'Show help') do
        puts opts
        exit
      end
    end

    opt_parser.parse!(options)
    args
  end

  def self.execute
    options = parse(ARGV)

    new(file: options.file, schema_versions: options.schema_versions, output_format: options.output_format).validate
  rescue OptionParser::InvalidOption => e
    logger.error('Invalid option: ', e)
    exit 1
  end
end

Main.execute
